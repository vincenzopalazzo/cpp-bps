"""
TODO insert license here
"""

import pandas
import json
# %matplotlib inline
import matplotlib as mpl
import matplotlib.pyplot as plt
from IPython.display import display_html


mpl.style.use('ggplot')

result_json_daily = open("../result-4.json", "r")
bm_json_daily = json.loads(result_json_daily.read())

result_json_lab = open("../result-256.json", "r")
bm_json_lab = json.loads(result_json_lab.read())

result_json_daily_jemalloc = open("../result-jemalloc-4.json", "r")
bm_json_daily_jemalloc = json.loads(result_json_daily_jemalloc.read())

result_json_lab_jemalloc = open("../result-jemalloc-256.json", "r")
bm_json_lab_jemalloc = json.loads(result_json_lab_jemalloc.read())

def make_chart(daily_machine: bool = False):
    if daily_machine is True:
        bm_json = bm_json_daily
    else:
        bm_json = bm_json_lab
    labels = []
    times_tseq = []
    times_tpar = []
    for bm in bm_json["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name'].split('/')[1]))
            times_tseq.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar.append(bm["real_time"])

    stl = pandas.DataFrame({
    'Tiskin Sequential Version': times_tseq,
    'Tiskin Parallel Version': times_tpar
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)

def make_chart_join():
    times_tseq_dell = []
    times_tpar_dell = []
    labels = []
    for bm in bm_json_daily["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name'].split('/')[1]))
            times_tseq_dell.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar_dell.append(bm["real_time"])
    
    times_tseq_lab = []
    times_tpar_lab = []        
    
    for bm in bm_json_lab["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            times_tseq_lab.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar_lab.append(bm["real_time"])

    stl = pandas.DataFrame({
    'Dell: Tiskin Sequential Version': times_tseq_dell,
    'Dell: Tiskin Parallel Version': times_tpar_dell,
    'Lab: Tiskin Sequential Version': times_tseq_lab,
    'Lab: Tiskin Parallel Version': times_tpar_lab,
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)
    
def make_chart_join_jemalloc(daily_machine: bool = False):
    if daily_machine is True:
        with_jemalloc = bm_json_daily_jemalloc
        without_jemalloc = bm_json_daily
    else:
        with_jemalloc = bm_json_lab_jemalloc
        without_jemalloc = bm_json_lab
    times_tseq_jemalloc = []
    times_tpar_jemalloc = []
    labels = []
    for bm in with_jemalloc["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name'].split('/')[1]))
            times_tseq_jemalloc.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar_jemalloc.append(bm["real_time"])
    
    times_tseq = []
    times_tpar = []        
    
    for bm in without_jemalloc["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            times_tseq.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar.append(bm["real_time"])

    stl = pandas.DataFrame({
    'JEMalloc: Tiskin Sequential Version': times_tseq_jemalloc,
    'Normal: Tiskin Sequential Version': times_tseq,
     'JEMalloc: Tiskin Parallel Version': times_tpar_jemalloc,
    'Normal: Tiskin Parallel Version': times_tpar,
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)
    
def make_chart_roadmap(daily_machine: bool = False):
    if daily_machine is True:
        bm_json = bm_json_daily
    else:
        bm_json = bm_json_lab
    labels = []
    times_tseq = []
    times_tpar = []
    for bm in bm_json["benchmarks"]:
        if "BM_CountingWordsSingle" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name']))
            times_tseq.append(bm["real_time"])
        elif "BM_CountingWordsMaxPar" in bm["name"]:
            times_tpar.append(bm["real_time"])

    stl = pandas.DataFrame({
    'Counting Word Sequential Version': times_tseq,
    'Counting Word Version': times_tpar
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)
    
def make_chart_roadmap_join():
    times_tseq_dell = []
    times_tpar_dell = []
    label = ["alice.txt"]
    for bm in bm_json_daily["benchmarks"]:
        if "BM_CountingWordsSingle" in bm["name"]:
            times_tseq_dell.append(bm["real_time"])
        elif "BM_CountingWordsMaxPar" in bm["name"]:
            times_tpar_dell.append(bm["real_time"])

    times_tseq_lab = []
    times_tpar_lab = []
    for bm in bm_json_lab["benchmarks"]:
        if "BM_CountingWordsSingle" in bm["name"]:
            times_tseq_lab.append(bm["real_time"])
        elif "BM_CountingWordsMaxPar" in bm["name"]:
            times_tpar_lab.append(bm["real_time"])
    
    stl = pandas.DataFrame({
       'Dell sequential algo': times_tseq_dell,
       'Dell parallel algo': times_tpar_dell,
       'Lab sequential algo': times_tseq_lab,
       'Lab parallel algo': times_tpar_lab,
       }, index=label)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)


def make_chart_sorting_alg(daily_machine: bool = False):
    if daily_machine is True:
        bm_json = bm_json_daily
    else:
        bm_json = bm_json_lab
    labels = []
    times = []
    for bm in bm_json["benchmarks"]:
        if "BM_CPP_SortingAlgorithm" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name'].split('/')[1]))
            times.append(bm["real_time"])

    stl = pandas.DataFrame({
    'C++ STL sorting': times,
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)

def make_chart_with_stl_time(daily_machine: bool = False):
    if daily_machine is True:
        bm_json = bm_json_daily
    else:
        bm_json = bm_json_lab
    labels = []
    times_tseq = []
    times_tpar = []
    times_stl = []
    for bm in bm_json["benchmarks"]:
        if "BM_Tiskin_Single/" in bm["name"]:
            labels.append('N: {}'.format(bm['run_name'].split('/')[1]))
            times_tseq.append(bm["real_time"])
        elif "BM_Tiskin_Par/" in bm["name"]:
            times_tpar.append(bm["real_time"])
        elif "BM_CPP_SortingAlgorithm" in bm["name"]:
            times_stl.append(bm["real_time"])
    stl = pandas.DataFrame({
    'Tiskin Sequential Version': times_tseq,
    'Tiskin Parallel Version': times_tpar,
    'C++ STL Sort Algorithm': times_stl,
    }, index=labels)
    stl.plot.bar(rot=0, figsize=(20,5), logy=True)