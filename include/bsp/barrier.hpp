#ifndef BARRIER_HPP
#define BARRIER_HPP

#include <condition_variable>
#include <mutex>
#include <thread>

#include "env.hpp"

namespace bsp::internal {
class Barrier {
private:
  std::size_t tasks;
  std::mutex access;
  std::condition_variable wakeup;

public:
  explicit Barrier(std::size_t core) : tasks(core) {}

  void notify() {
    std::unique_lock<std::mutex> lock(this->access);
    tasks--;
    wakeup.notify_all();
  }

  void wait() {
    while (tasks > 0) {
      std::this_thread::yield();
    }
  }
};
}; // namespace bsp::internal

#endif
