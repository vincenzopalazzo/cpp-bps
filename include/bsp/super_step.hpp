#ifndef SUPER_STEP_H
#define SUPER_STEP_H

#include <memory>

#include "activity.hpp"
#include "barrier.hpp"
#include "env.hpp"

// Implementation of the Super Step concept in the theoretical
// description of the BSP pattern
namespace bsp {
template <class In> class SuperStep {
private:
  // Each activity has a list of activites that the
  // final user can fill with the method add_activity
  std::vector<std::shared_ptr<bsp::Activity<In>>> activites;

public:
  // If the user know the size of the activites
  // the user can pass the size to reserve the value in the vector.
  SuperStep(std::size_t size = 0) { this->activites.reserve(size); }

  void add_activity(std::shared_ptr<bsp::Activity<In>> activity) {
    this->activites.push_back(std::move(activity));
  }

  // Method call by the BSP class and it is the core of the logic to run
  // the activity in parallel.
  // For run the for in parallel the OMP lib is used.
  void execute(std::shared_ptr<bsp::Env<In>> env) {
    auto degree = activites.size() > env->core_to_use() ? env->core_to_use()
                                                        : activites.size();
    // The Barrier is not necessary in this case
    // auto barrier =
    // std::make_shared<bsp::internal::Barrier>(activites.size());
#pragma omp parallel for num_threads(degree)
    for (auto const &activity : activites) {
      activity->run(env);
      // barrier->notify();
    }
    // barrier->wait();
  }
};
}; // namespace bsp

#endif
