#ifndef ENV_H
#define ENV_H

#include <thread>

#include <cpstl/HashMap.hpp>

#include "msg.hpp"

// Environment concept implementation, this is the the shared object between
// all the BSP pattern. This class contains all the info for system and the
// global repository to share message between super steps.
namespace bsp {
template <typename T = int> class Env {
protected:
  std::size_t available_cores;
  std::size_t used_core;
  cpstl::thread_safe::HashMap<int, bsp::Msg<T>> messages;
  std::size_t incremental_id;
  std::size_t prev_id_size;

public:
  explicit Env() {
    this->available_cores = std::thread::hardware_concurrency();
    this->used_core = this->available_cores;
    this->incremental_id = 0;
    this->prev_id_size = 0;
  }

  explicit Env(std::size_t to_use) {
    Env();
    this->used_core = to_use;
  }

  inline std::size_t core_to_use() const { return used_core; }

  void add_message(int const activity_id, bsp::Msg<T> const &msg) {
    this->messages.add(activity_id, msg);
  }

  bool contains_message(int const activity_id) {
    return this->messages.contains(activity_id);
  }

  // In this case we can copy the pointer, without worry about the
  // concurrency of the access because the instance of activity I is unique
  // in the pattern.
  std::queue<bsp::Msg<T>> get_message(int const activity_id) {
    auto value = this->messages.get(activity_id);
    this->messages.remove(activity_id);
    return value;
  }

  inline std::size_t get_incremental_id() {
    // only to make the code clear
    // we increment the id by one and return re previous value.
    return this->incremental_id++;
  }

  void reset_activity_id() {
    this->prev_id_size = this->incremental_id;
    this->incremental_id = 0;
  }
};
}; // namespace bsp

#endif
