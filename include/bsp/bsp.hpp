#ifndef BSP_H
#define BSP_H

#include <memory>
#include <vector>

#include "barrier.hpp"
#include "env.hpp"
#include "super_step.hpp"

namespace bsp {
template <class In> class BSP {
private:
  std::vector<SuperStep<In>> super_steps;
  std::shared_ptr<bsp::Env<In>> sys_env;

public:
  BSP(std::shared_ptr<bsp::Env<In>> env) { sys_env = env; }

  void add_super_step(SuperStep<In> &super_steps) {
    // avoid the copy of the pointer.
    this->super_steps.push_back(std::move(super_steps));
  }

  void execute() {
    for (auto &super_step : super_steps) {
      super_step.execute(sys_env);
    }
  }
};
}; // namespace bsp

#endif
