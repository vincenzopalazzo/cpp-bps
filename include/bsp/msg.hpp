#ifndef MSG_H
#define MSG_H

#include <memory>

// Simple Class to make the Message concept, the message
// in this case contains the id of the activity that we want achieve
// and the value for the next super step.
namespace bsp {
template <class T> class Msg {
public:
  int activity_id;
  // It is better to make a share pointer and share the reference
  // of the pointer, instead of the value.
  // With this method we avoid the copy.
  T value;

  Msg(int activity_id, T const &value)
      : activity_id(activity_id), value(value) {}
};
}; // namespace bsp

#endif
