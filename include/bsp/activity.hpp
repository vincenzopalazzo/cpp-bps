#ifndef ACTIVITY_H
#define ACTIVITY_H

#include "barrier.hpp"
#include "env.hpp"
#include "super_step.hpp"

// Activity class is a implementation of the activity concept of the
// BSP pattern theoretical description.
// It is an abstract class that the end user need to implement the method run.
namespace bsp {
template <class T> class Activity {
private:
  // FIXME(vincenzopalazzo): Remove this declaration
  // for how we set the interface of the BSP pattern
  // we can say that we don't need to wait the termination
  // of all the process because we ran the super-step in sequence.
  // std::shared_ptr<bsp::internal::Barrier> barrier;

  // The super step class can access to the method private and
  // protected of the class, this is not good with the actual
  // implementation, but is possible abstract the implementation
  // of an activity where each activity receive the queue of the message
  // and not the environment object.
  template <class> friend class SuperStep;

protected:
  // each activity have an id where the final user
  // can choose how give this type of id.
  std::size_t activity_id;

public:
  Activity(std::size_t activity_id) : activity_id(activity_id) {}

  ~Activity() = default;

  // The only abstract method of the class, that the final user
  // need to implement and fill with the algorithm logic.
  // TODO: The Env here is a shared ptr, but with the actual implementation
  // it can be moved with a stack allocation and pass the reference.
  // What are the drowback?
  virtual void run(std::shared_ptr<bsp::Env<T>> env) = 0;

  inline std::size_t get_activity_id() { return activity_id; }
};
}; // namespace bsp

#endif
