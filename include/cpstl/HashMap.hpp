/**
 * Thread safe Hash Map C++ 17 implementation
 * Copyright (C) 2020  Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

#ifndef HASH_MAP_H
#define HASH_MAP_H

#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <exception>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <shared_mutex>

#include "UniversalHash.hpp"

namespace cpstl {

namespace internal {
template <typename Key, typename Value> class Bucket {
private:
  std::list<std::pair<Key, std::queue<Value>>> data;
  mutable std::shared_mutex mutex;

  typename std::list<std::pair<Key, std::queue<Value>>>::iterator
  find_with_key(Key const &key) {
    return std::find_if(data.begin(), data.end(),
                        [&](std::pair<Key, std::queue<Value>> elem) {
                          return elem.first == key;
                        });
  }

public:
  std::queue<Value> get(Key const &key) {
    std::shared_lock lock(mutex);
    auto iter = find_with_key(key);
    assert(iter != data.end());
    return iter->second;
  }

  void add(Key const &key, Value const &value) {
    std::unique_lock lock(mutex);
    auto iter = find_with_key(key);
    if (iter == data.end()) {
      std::queue<Value> tmp;
      tmp.push(value);
      data.emplace_back(key, tmp);
    } else
      iter->second.push(value);
  }

  bool contains(Key const &key) {
    std::shared_lock lock(mutex);
    auto iter = find_with_key(key);
    return iter != data.end();
  }

  void remove(Key const &key) {
    std::unique_lock lock(mutex);
    auto iter = find_with_key(key);
    if (iter != data.end())
      data.erase(iter);
  }
};
}; // namespace internal

namespace thread_safe {
template <typename Key, typename Value,
          typename Hash = cpstl::UniversalHash<Key>>
class HashMap {
private:
  std::vector<std::unique_ptr<cpstl::internal::Bucket<Key, Value>>> buckets;
  Hash hash_function;

  cpstl::internal::Bucket<Key, Value> &get_bucket(Key const &key) const {
    auto index = hash_function.universal_hashing(key);
    return *buckets[index];
  }

public:
  HashMap(std::size_t bucket_size = 19,
          Hash const &hash_function = UniversalHash<Key>(19))
      : buckets(bucket_size), hash_function(hash_function) {
    for (std::size_t i = 0; i < bucket_size; i++)
      buckets[i].reset(new cpstl::internal::Bucket<Key, Value>());
  }

  HashMap(HashMap const &other) = delete;
  HashMap &operator=(HashMap const &other) = delete;

  std::queue<Value> get(Key const &key) { return get_bucket(key).get(key); }

  bool contains(Key const &key) { return get_bucket(key).contains(key); }

  void add(Key const &key, Value const &value) {
    get_bucket(key).add(key, value);
  }

  void remove(Key const &key) { get_bucket(key).remove(key); }
};
}; // namespace thread_safe
}; // namespace cpstl

#endif
