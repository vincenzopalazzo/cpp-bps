#include <string>
#include <vector>

#include <gtest/gtest.h>

#include <bsp/bsp.hpp>
#include <bsp/env.hpp>

#include "./utils/counting_words.hpp"

using namespace bsp;
using namespace bsp::mock;

TEST(BSP_TEST, AddingStep) {
  auto env =
      std::make_shared<bsp::Env<int>>(); // Make a env with the default conf
  bsp::BSP<int> bsp(env);

  // Fist step, load the words and split into words
  bsp::SuperStep<int> load_words;
  // add activity, we need a method to load this activity, like a collector?
  // see fast flow.
  std::shared_ptr<Activity<int>> elem = std::make_shared<LoadWords<int>>(1);
  load_words.add_activity(elem);
  bsp.add_super_step(load_words);
  bsp.execute();
}

TEST(BSP_TEST, AddingMessageToStep) {
  auto env =
      std::make_shared<bsp::Env<int>>(); // Make a env with the default conf
  bsp::BSP<int> bsp(env);

  // Fist step, load the words and split into words
  bsp::SuperStep<int> load_words;
  // add activity, we need a method to load this activity, like a collector?
  // see fast flow.
  std::shared_ptr<Activity<int>> elem = std::make_shared<LoadWords<int>>(1);

  load_words.add_activity(elem);
  auto id = elem->get_activity_id();
  // the shared is dead by ownership

  auto value = 0;
  auto msg = bsp::Msg<int>(id, value);
  env->add_message(id, msg);

  bsp.add_super_step(load_words);
  bsp.execute();

  auto msg_queue = env->get_message(id + 1);
  msg = msg_queue.front();
  msg_queue.pop();
  ASSERT_EQ(1, msg.value);
}

TEST(BSP_TEST, AddingMessageWithRes) {
  auto env =
      std::make_shared<bsp::Env<int>>(); // Make a env with the default conf
  bsp::BSP<int> bsp(env);

  // Fist step, load the words and split into words
  bsp::SuperStep<int> load_words;
  // add activity, we need a method to load this activity, like a collector?
  // see fast flow.
  std::shared_ptr<Activity<int>> elem = std::make_shared<LoadWords<int>>(1);

  load_words.add_activity(elem);
  auto id = elem->get_activity_id();
  // the shared is dead by ownership

  auto value = 0;
  auto msg = bsp::Msg<int>(id, value);
  env->add_message(id, msg);

  bsp.add_super_step(load_words);
  bsp.execute();

  auto queue_msg = env->get_message(id + 1);
  auto front = queue_msg.front();
  ASSERT_TRUE(front.value == 1);
}

TEST(BSP_TEST, TwoSuperStepOneQueue) {
  auto env =
      std::make_shared<bsp::Env<int>>(); // Make a env with the default conf
  bsp::BSP<int> bsp(env);

  // Fist step, load the words and split into words
  bsp::SuperStep<int> load_words;
  bsp::SuperStep<int> double_words;
  // add activity, we need a method to load this activity, like a collector?
  // see fast flow.
  std::shared_ptr<Activity<int>> elem = std::make_shared<LoadWords<int>>(1);
  std::shared_ptr<Activity<int>> doubled =
      std::make_shared<DoubledValue<int>>(2);

  load_words.add_activity(elem);
  double_words.add_activity(doubled);

  auto elem_id = elem->get_activity_id();
  auto double_id = doubled->get_activity_id();
  // the shared is dead by ownership

  auto value = 0;
  auto msg = bsp::Msg<int>(elem_id, value);
  env->add_message(elem_id, msg);

  bsp.add_super_step(load_words);
  bsp.add_super_step(double_words);

  bsp.execute();

  auto queue_msg = env->get_message(double_id + 1);
  ASSERT_EQ(1, queue_msg.size());
  auto front = queue_msg.front();
  ASSERT_EQ(10, front.value);
}

TEST(BSP_TEST, CountingWords) {
  auto env = std::make_shared<bsp::Env<std::vector<std::string>>>();
  bsp::BSP<std::vector<std::string>> bsp(env);

  // Read the file and put the vector in the queue.
  auto file = std::fstream("data/alice.txt");
  std::vector<std::string> words;
  std::string word;
  while (file >> word) {
    words.push_back(word);
  }

  bsp::Msg<std::vector<std::string>> msg(0, words);
  env->add_message(msg.activity_id, msg);

  // Get the parallel degree
  auto degree = words.size() / env->core_to_use();

  bsp::SuperStep<std::vector<std::string>> emitter;

  std::shared_ptr<Activity<std::vector<std::string>>> emitter_act =
      std::make_shared<Emitter<std::vector<std::string>>>(
          env->get_incremental_id()); // Use the automatic id

  emitter.add_activity(emitter_act);
  bsp.add_super_step(emitter);

  bsp::SuperStep<std::vector<std::string>> count_word;

  for (std::size_t i = 1; i <= degree; i++) {
    std::shared_ptr<Activity<std::vector<std::string>>> counter_act =
        std::make_shared<Counter<std::vector<std::string>>>(i);
    count_word.add_activity(counter_act);
  }
  bsp.add_super_step(count_word);

  bsp::SuperStep<std::vector<std::string>> printer;

  std::shared_ptr<Activity<std::vector<std::string>>> printer_act =
      std::make_shared<Printer<std::vector<std::string>>>(99999);

  printer.add_activity(printer_act);

  bsp.add_super_step(printer);

  bsp.execute();

  // Read the file and put the vector in the queue.
  file = std::fstream("data/result.txt");
  std::vector<std::string> result;
  while (file >> word) {
    result.push_back(word);
  }

  ASSERT_TRUE(result.size() < words.size());
}
