#include <gtest/gtest.h>

#include <bsp/barrier.hpp>
#include <bsp/env.hpp>

TEST(BARRIER_TEST_SIN, barrier_usage_with_success) {
  auto machine_thread = std::thread::hardware_concurrency();
  auto env = bsp::Env();
  bsp::internal::Barrier barrier(env.core_to_use());

  for (std::size_t i = 0; i < machine_thread; i++)
    barrier.notify();
  barrier.wait();
  ASSERT_TRUE(true);
}

// TODO: This need to be moved in an external function
static void notify(bsp::internal::Barrier &barrier) { barrier.notify(); }

TEST(BARRIER_TEST_ASIN, barrier_usage_with_success) {
  auto machine_thread = std::thread::hardware_concurrency();
  auto env = bsp::Env();
  bsp::internal::Barrier barrier(env.core_to_use());

  std::vector<std::thread> pool;
  pool.reserve(machine_thread);

  for (std::size_t i = 0; i < machine_thread; i++) {
    std::thread work = std::thread(notify, std::ref(barrier));
    pool.push_back(std::move(work));
  }

  for (auto &work : pool)
    work.join();

  barrier.wait();
  ASSERT_TRUE(true);
}
