#set(HEADER_LIST "${cpp-bps_SOURCE_DIR}/include/bsp/bsp.hpp")

# Make an automatic library - will be static or dynamic based on user setting
file(GLOB HEADER_LIST CONFIGURE_DEPENDS
  "${cpp-bps_SOURCE_DIR}/include/*.hpp")

# TODO: Check if the library exist
find_package(OpenMP)

add_library(bsp_dep include.cpp ${HEADER_LIST})

# We need this directory, and users of our library will need it too
target_include_directories(bsp_dep PUBLIC ../include)

target_link_libraries(bsp_dep OpenMP::OpenMP_CXX)

# This depends on (header only) boost
#target_link_libraries(modern_library PRIVATE Boost::boost)

# All users of this library will need at least C++11
target_compile_features(bsp_dep PUBLIC cxx_std_17)

# IDEs should put the headers in a nice place
source_group(
  TREE "${PROJECT_SOURCE_DIR}/include"
  PREFIX "Header Files"
  FILES ${HEADER_LIST})
