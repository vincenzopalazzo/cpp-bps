#include <algorithm>
#include <iostream>
#include <random>

#include <benchmark/benchmark.h>

#include <bsp/bsp.hpp>
#include <bsp/env.hpp>

#include "counting_words.hpp"
#include "tiskin.hpp"

using namespace bsp::counting;
using namespace bsp;

template <class T> static bsp::BSP<T> setUpCountingWork(int parallel_degree);

template <class T>
static bsp::BSP<T> setUpTikinAlgo(std::size_t item_size, int parallel_degree);

template <class T>
static bsp::BSP<T> setUpTikinAlgoSlim(std::size_t item_size,
                                      int parallel_degree);

static void BM_CountingWordsSingle(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp = setUpCountingWork<std::vector<std::string>>(1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_CountingWordsMaxPar(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp = setUpCountingWork<std::vector<std::string>>(-1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_Tiskin_Single(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp = setUpTikinAlgo<std::vector<int>>((std::size_t)state.range(0),
                                                (std::size_t)1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_Tiskin_Par_Slim(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp =
        setUpTikinAlgoSlim<std::vector<int>>((std::size_t)state.range(0), -1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_Tiskin_Single_Slim(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp = setUpTikinAlgoSlim<std::vector<int>>((std::size_t)state.range(0),
                                                    (std::size_t)1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_Tiskin_Par(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    auto bsp =
        setUpTikinAlgo<std::vector<int>>((std::size_t)state.range(0), -1);
    state.ResumeTiming();
    bsp.execute();
  }
}

static void BM_CPP_SortingAlgorithm(benchmark::State &state) {
  // Perform setup here
  std::srand(unsigned(std::time(nullptr)));
  for (auto _ : state) {
    // This code gets timed
    state.PauseTiming();
    std::vector<int> inputs(state.range(0));
    std::generate(inputs.begin(), inputs.end(), std::rand);
    state.ResumeTiming();
    std::sort(inputs.begin(), inputs.end());
  }
}

template <class T>
static bsp::BSP<T> setUpTikinAlgo(std::size_t item_size, int parallel_degree) {
  std::shared_ptr<bsp::Env<T>> env;
  if (parallel_degree > 0)
    env = std::make_shared<bsp::Env<T>>(parallel_degree);
  else
    env = std::make_shared<bsp::Env<T>>();
  bsp::BSP<T> bsp(env);

  std::random_device rd;
  std::mt19937_64 mt(rd());
  auto generator = std::uniform_int_distribution<int>(1, item_size);
  T input;
  std::generate(input.begin(), input.end(), [&]() { return generator(mt); });

  bsp::Msg<T> msg(0, input);
  env->add_message(0, msg);

  bsp::SuperStep<T> emitter;
  std::shared_ptr<bsp::tiskin::Emitter<T>> emitter_act =
      std::make_shared<bsp::tiskin::Emitter<T>>(0);
  emitter.add_activity(emitter_act);

  bsp.add_super_step(emitter);

  bsp::SuperStep<T> step_one;

  auto par_degree = item_size / env->core_to_use();
  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_one_act =
        std::make_shared<bsp::tiskin::StepOne<T>>(i, par_degree);
    step_one.add_activity(step_one_act);
  }

  bsp.add_super_step(step_one);

  bsp::SuperStep<T> step_two;

  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_two_act =
        std::make_shared<bsp::tiskin::StepTwo<T>>(i, par_degree);
    step_two.add_activity(step_two_act);
  }

  bsp.add_super_step(step_two);

  bsp::SuperStep<T> step_three;

  for (std::size_t i = 1; i <= par_degree; i++) {
    // TODO make a meaning of the collector id.
    auto step_three_act = std::make_shared<bsp::tiskin::StepThree<T>>(i);
    step_two.add_activity(step_three_act);
  }

  bsp.add_super_step(step_three);

  return bsp;
}

template <class T>
static bsp::BSP<T> setUpTikinAlgoSlim(std::size_t item_size,
                                      int parallel_degree) {
  std::shared_ptr<bsp::Env<T>> env;
  if (parallel_degree > 0)
    env = std::make_shared<bsp::Env<T>>(parallel_degree);
  else
    env = std::make_shared<bsp::Env<T>>();
  bsp::BSP<T> bsp(env);

  std::random_device rd;
  std::mt19937_64 mt(rd());
  auto generator = std::uniform_int_distribution<int>(1, item_size);
  T input;
  std::generate(input.begin(), input.end(), [&]() { return generator(mt); });

  std::size_t id = 1;
  std::size_t index = 0;
  auto par_degree = item_size / env->core_to_use();
  T chunk;
  chunk.reserve(par_degree);
  for (auto const &elem : input) {
    if (index == par_degree) {
      chunk.push_back(elem);
      bsp::Msg<T> msg(id++, chunk);
      env->add_message(msg.activity_id, msg);
      chunk.clear();
      chunk.reserve(par_degree);
      index = 0;
      continue;
    }
    chunk.push_back(elem);
    index++;
  }

  bsp::SuperStep<T> step_one;

  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_one_act =
        std::make_shared<bsp::tiskin::StepOne<T>>(i, par_degree);
    step_one.add_activity(step_one_act);
  }

  bsp.add_super_step(step_one);

  bsp::SuperStep<T> step_two;

  for (std::size_t i = 1; i <= par_degree; i++) {
    auto step_two_act =
        std::make_shared<bsp::tiskin::StepTwo<T>>(i, par_degree);
    step_two.add_activity(step_two_act);
  }

  bsp.add_super_step(step_two);

  bsp::SuperStep<T> step_three;

  for (std::size_t i = 1; i <= par_degree; i++) {
    // TODO make a meaning of the collector id.
    auto step_three_act = std::make_shared<bsp::tiskin::StepThree<T>>(i);
    step_two.add_activity(step_three_act);
  }

  bsp.add_super_step(step_three);

  return bsp;
}

template <class T> static bsp::BSP<T> setUpCountingWork(int parallel_degree) {
  std::shared_ptr<bsp::Env<T>> env;
  if (parallel_degree > 0) {
    env = std::make_shared<bsp::Env<T>>(parallel_degree);
  } else {
    env = std::make_shared<bsp::Env<T>>();
  }
  bsp::BSP<T> bsp(env);

  // Read the file and put the vector in the queue.
  auto file = std::fstream("data/alice.txt");
  std::vector<std::string> words;
  std::string word;
  while (file >> word) {
    words.push_back(word);
  }

  bsp::Msg<T> msg(0, words);
  env->add_message(msg.activity_id, msg);

  // Get the parallel degree
  auto degree = words.size() / env->core_to_use();

  bsp::SuperStep<T> emitter;

  std::shared_ptr<Activity<T>> emitter_act = std::make_shared<Emitter<T>>(
      env->get_incremental_id()); // Use the automatic id

  emitter.add_activity(emitter_act);
  bsp.add_super_step(emitter);

  bsp::SuperStep<T> count_word;

  for (std::size_t i = 1; i <= degree; i++) {
    std::shared_ptr<Activity<T>> counter_act = std::make_shared<Counter<T>>(i);
    count_word.add_activity(counter_act);
  }
  bsp.add_super_step(count_word);

  bsp::SuperStep<T> printer;

  std::shared_ptr<Activity<T>> printer_act =
      std::make_shared<Printer<T>>(99999);

  printer.add_activity(printer_act);

  bsp.add_super_step(printer);

  return bsp;
}

// Register the function as a benchmark
BENCHMARK(BM_CountingWordsSingle);
BENCHMARK(BM_CountingWordsMaxPar);
BENCHMARK(BM_Tiskin_Single)->Range(8, 8 << 20);
BENCHMARK(BM_Tiskin_Par)->Range(8, 8 << 20);
BENCHMARK(BM_CPP_SortingAlgorithm)->Range(8, 8 << 20);
BENCHMARK(BM_Tiskin_Single_Slim)->Range(8, 8 << 20);
BENCHMARK(BM_Tiskin_Par_Slim)->Range(8, 8 << 20);

// Run the benchmark
BENCHMARK_MAIN();
