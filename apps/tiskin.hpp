#ifndef TISKIN_H
#define TISKIN_H

#include <algorithm>

#include <bsp/activity.hpp>
#include <bsp/msg.hpp>

namespace bsp::tiskin {

// split in_vector in p ranges by computing p+1 samples, and return
// the p+1 samples equally spaced
template <typename T>
static std::vector<T> split_with_samples(std::vector<T> const &inputs,
                                         std::size_t par_degree) {
  std::vector<T> tmp;

  auto range = inputs.size() / (par_degree + 1);
  auto extra = inputs.size() % (par_degree + 1);
  auto prev = inputs.begin();
  tmp.reserve(extra + par_degree);
  for (std::size_t i = 0; i < extra; i++) {
    tmp.push_back(*prev);
    prev += (range + 1);
  }

  for (std::size_t i = 0; i < (par_degree - extra); i++) {
    tmp.push_back(*prev);
    prev += range;
  }
  tmp.push_back(*(inputs.end() - 1));

  return tmp;
}

template <class T> class Emitter : public bsp::Activity<T> {
public:
  Emitter(std::size_t id) : bsp::Activity<T>(id) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() == 1 && "Too message in the queue");
      auto msg = msg_queue.front();
      msg_queue.pop();
      auto par_degree = msg.value.size() / env->core_to_use();
      std::size_t index = 0;
      std::size_t next_activity = this->activity_id + 1;
      T chunck;
      chunck.reserve(par_degree);
      for (auto &elem : msg.value) {
        if (index == par_degree) {
          chunck.push_back(elem);
          bsp::Msg<T> msg(next_activity++, chunck);
          env->add_message(msg.activity_id, msg);
          chunck.clear();
          chunck.reserve(par_degree);
          index = 0;
          continue;
        }
        chunck.push_back(elem);
        index++;
      }
    }
  }
};

template <class T> class StepOne : public bsp::Activity<T> {
private:
  std::size_t par_degree;

public:
  StepOne(std::size_t id, std::size_t par_degree)
      : bsp::Activity<T>(id), par_degree(par_degree) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() > 0 && "Message queue empty");
      for (std::size_t i = 0; i < msg_queue.size(); i++) {
        auto msg = msg_queue.front();
        std::sort(msg.value.begin(), msg.value.end());

        auto samples = split_with_samples(msg.value, this->par_degree);
        msg.value = samples;
        // add message for the next step.
        env->add_message(this->activity_id, msg);
        msg_queue.pop();
      }
    }
  }
};

template <class T> class StepTwo : public bsp::Activity<T> {
private:
  std::size_t par_degree;

public:
  StepTwo(std::size_t id, std::size_t par_degree)
      : bsp::Activity<T>(id), par_degree(par_degree) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() > 0 && "Message queue empty");
      for (std::size_t i = 0; i < msg_queue.size(); i++) {
        auto msg = msg_queue.front();
        msg_queue.pop();

        auto samples = split_with_samples(msg.value, this->par_degree);
        msg.value = samples;
        std::sort(msg.value.begin(), msg.value.end());
        env->add_message(this->activity_id, msg);
      }
    }
  }
};

template <class T> class StepThree : public bsp::Activity<T> {
public:
  StepThree(std::size_t id) : bsp::Activity<T>(id) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      // TODO: The logic inside this method can be avoided
      // and the message queue can be pass from the super step
      // in this case the process can be a little bit complicated
      // because we need to manage the case when an activity want
      // to join some result.
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() > 0 && "Message queue empty");
      for (std::size_t i = 0; i < msg_queue.size(); i++) {
        auto msg = msg_queue.front();
        msg_queue.pop();
        std::sort(msg.value.begin(), msg.value.end());
        env->add_message(this->activity_id, msg);
      }
    }
  }
};
}; // namespace bsp::tiskin

#endif
