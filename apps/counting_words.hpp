#ifndef COUNTWORDS_H
#define COUNTWORDS_H

#include <bsp/activity.hpp>
#include <bsp/msg.hpp>
#include <fstream>

namespace bsp::counting {

template <class T> class Emitter : public bsp::Activity<T> {
public:
  Emitter(std::size_t id) : bsp::Activity<T>(id) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() == 1);
      auto msg = msg_queue.front();
      msg_queue.pop();
      auto degree = msg.value.size() / env->core_to_use();
      std::size_t index = 0;
      std::size_t next_activity = this->activity_id + 1;
      std::vector<std::string> for_msg;
      for (auto const &elem : msg.value) {
        if (index == degree) {
          for_msg.push_back(elem);
          bsp::Msg<T> msg(next_activity++, for_msg);
          env->add_message(msg.activity_id, msg);
          for_msg = std::vector<std::string>();
          index = 0;
          continue;
        }
        for_msg.push_back(elem);
        index++;
      }
    }
  }
};

template <class T> class Counter : public bsp::Activity<T> {

public:
  Counter(std::size_t id) : bsp::Activity<T>(id) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto msg_queue = env->get_message(this->activity_id);
      assert(msg_queue.size() == 1);
      auto msg = msg_queue.front();
      std::map<std::string, int> counts{};
      for (auto &word : msg.value) {
        if (counts.find(word) != counts.end()) {
          counts[word]++;
        } else {
          counts[word] = 1;
        }
      }
      std::vector<std::string> result;
      result.reserve(counts.size());
      for (auto &[key, value] : counts) {
        result.push_back(key + ":" + std::to_string(value));
      }
      bsp::Msg<std::vector<std::string>> new_msg(99999, result);
      env->add_message(new_msg.activity_id, new_msg);
    }
  }
};

template <class T> class Printer : public bsp::Activity<T> {

public:
  Printer(std::size_t id) : bsp::Activity<T>(id) {}

  void run(std::shared_ptr<bsp::Env<T>> env) override {
    if (env->contains_message(this->activity_id)) {
      auto queue_msg = env->get_message(this->activity_id);
      std::ofstream result;
      result.open("data/result.txt");
      for (std::size_t i = 0; i < queue_msg.size(); i++) {
        auto msg = queue_msg.front();
        for (auto &word : msg.value)
          result << word << "\n";
        queue_msg.pop();
      }
      result.close();
    }
  }
};
}; // namespace bsp::counting

#endif
